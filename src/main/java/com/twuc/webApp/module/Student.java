package com.twuc.webApp.module;

import javax.validation.constraints.*;

public class Student {
    @Size(min = 0,max = 5)
    private String name;
    @NotNull
    private String gender;
    @Max(10)
    @Min(1)
    private Integer klass;
    @Email
    private String email;

    public Student(@Size(min = 0, max = 5) String name, @NotNull String gender, @Max(10) @Min(1) Integer klass, @Email String email) {
        this.name = name;
        this.gender = gender;
        this.klass = klass;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getKlass() {
        return klass;
    }

    public void setKlass(Integer klass) {
        this.klass = klass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
