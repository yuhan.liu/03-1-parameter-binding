package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.module.Contract;
import com.twuc.webApp.module.DateTime;
import com.twuc.webApp.module.Student;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class TwuTrainingController {
    private ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping("/api/Integer/{userId}")
    public String pathVariableChange(@PathVariable Integer userId) {
        return userId.toString();
    }

    @GetMapping("/api/int/{userId}")
    public String pathVariableChange(@PathVariable int userId) {
        return "return" + userId;
    }

    @GetMapping("/api/users/{userId}/books/{bookId}")
    public String returnTwoPathVariable(@PathVariable String userId, @PathVariable String bookId) {
        return userId + " and " + bookId;
    }

//    @PostMapping("/student")
//    public String createNewStudent(@RequestBody @Valid Student student) {
//        return student.getName();
//    }

    @GetMapping("/api/user")
    public String getStudent(@RequestParam(value = "name", defaultValue = "bob") String name,
                             @RequestParam(value = "class", defaultValue = "23") String klass) {
        return name + " from " + klass;
    }

    @GetMapping("/api/user/collection")
    public String getStudentIds(@RequestParam List<String> studentId) {
        return String.valueOf(studentId);
    }

    @PostMapping("/api/contract/serialize")
    public String serializeContract(@RequestBody Contract contract) throws JsonProcessingException {
        return objectMapper.writeValueAsString(contract);
    }

    @GetMapping("/api/contract/deserialize")
    public Contract deserializeContract(@RequestParam String json) throws IOException {
        return objectMapper.readValue(json, Contract.class);
    }

    @PostMapping("/api/student/serialize")
    public String serializeStudent(@RequestBody Student student) throws JsonProcessingException {
        return objectMapper.writeValueAsString(student);
    }

    @PostMapping("/api/datetimes")
    public DateTime deserializeTime(@RequestBody DateTime dataTime) {
        return dataTime;
    }

    @PostMapping("/api/student")
    public String createPerson(@RequestBody @Valid Student student) {
        return student.getName();
    }

}