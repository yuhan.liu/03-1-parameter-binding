package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.module.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class TwuTrainingControllerTest {
    @Autowired
    MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

//    @Test
//    void should_get_json() throws Exception {
//        mockMvc.perform(post("/student")
//                .content("{\"name\":\"xiaoli\"}")
//                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
//        )
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.content().string("xiaoli"))
//                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN_VALUE));
//    }

    @Test
    void PathVariable_Integer_type() throws Exception {
        mockMvc.perform(get("/api/Integer/23"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().string("23"));
    }

    @Test
    void PathVariable_int_type() throws Exception {
        mockMvc.perform(get("/api/int/23"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().string("return23"));
    }

    @Test
    void should_return_userId_and_bookId() throws Exception {
        mockMvc.perform(get("/api/users/23/books/543"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().string("23 and 543"));
    }

    @Test
    void should_return_user_message() throws Exception {
        mockMvc.perform(get("/api/user")
                .param("name", "lily")
                .param("class", "55"))
                .andExpect(status().isOk())
                .andExpect(content().string("lily from 55"));
    }

    @Test
    void should_return_default_user_information_when_give_username_and_class() throws Exception {
        mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andExpect(content().string("bob from 23"));
    }

    @Test
    void should_return_all_user_information_when_give_collection() throws Exception {
        mockMvc.perform(get("/api/user/collection").param("studentId", "1, 2, 3, 4"))
                .andExpect(status().isOk())
                .andExpect(content().string("[1, 2, 3, 4]"));
    }

    @Test
    void should_return_json_message_in_serialize() throws Exception {
        mockMvc.perform(post("/api/contract/serialize")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{\"name\":\"tom\",\"gender\":\"female\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("tom"));
    }

    @Test
    void should_return_object_message_in_deserialize() throws Exception {
        mockMvc.perform(get("/api/contract/deserialize")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param("json", "{\"name\":\"lily\",\"gender\":\"female\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("lily"))
                .andExpect(jsonPath("$.gender").value("female"));
    }

    @Test
    void should_return_object_message_have_no_setter() throws Exception {
        mockMvc.perform(post("/api/student/serialize")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{\"name\":\"lily\",\"gender\":\"female\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("lily"));
    }

    @Test
    void should_return_local_datatime() throws Exception {
        mockMvc.perform(post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_error_when_not_ISO_dateTime() throws Exception {
        mockMvc.perform(post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{ \"dateTime\": \"2019-10-01\" }"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_return_the_gender_when_input_is_not_null() throws Exception {
        Student student = new Student("lily", "female", 9, "xxx@gmail.com");
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(student)))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_the_gender_when_input_is_null() throws Exception {
        Student student = new Student("lily", null, 9, "xxx@gmail.com");
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(student)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_the_person_name_when_name_size_is_ok() throws Exception {
        Student student = new Student("lily", "female", 9, "bohenmian@gmail.com");
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(student)))
                .andExpect(status().isOk())
                .andExpect(content().string("lily"));
    }

    @Test
    void should_throw_error_code_when_class_more_than_10() throws Exception {
        Student student = new Student("lily", "female", 12, "xxx@gmail.com");
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(student)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_throw_error_code_when_input_email_is_invalid() throws Exception {
        Student student = new Student("lily", "female", 9, "xxx.com");
        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(student)))
                .andExpect(status().isBadRequest());
    }
}
